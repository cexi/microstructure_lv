# microstructure_LV
Collagen_CPP: contains user-defined C++  expression of  second Piola-Kirchhoff stress (Sij) and elasticity tensor(Cijkl) for collagen fibers

Muscle_CPP: contains user-defined C++  expression of second Piola-Kirchhoff stress (Sij) and elasticity tensor(Cijkl) for muscle fibers

Tortuosity: contains code for calculating the collagen fiber tortuosity



You need to first install FEniCS  or use the corresponding docker image (version 2017.1.0 preferred) to run this code.

In order to run the code in parallel, you can use the following command:

mpirun.mpich -np 16 python passive_inflation.py  

