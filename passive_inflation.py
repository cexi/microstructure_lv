from dolfin import * 
import numpy as np
from petsc4py import PETSc
from scipy import integrate
import math
from datetime import datetime
from edgetypebc import *

startTime = datetime.now()

deg = 2
parameters["form_compiler"]["representation"] ="uflacs"
parameters["form_compiler"]["quadrature_degree"]=deg


def Fmat(u):

    d = u.geometric_dimension()
    I = Identity(d)
    F = I + grad(u)
    return F

def Emat(u):
 
    d = u.geometric_dimension()
    I = Identity(d)
    F = Fmat(u)
    return 0.5*(F.T*F-I)



# Parallel reading of mesh ###################################################
meshname = "ellipsoidal"
mesh = Mesh()
f = HDF5File(mpi_comm_world(), meshname+".hdf5", 'r')
f.read(mesh, meshname, False)


edgeboundaries = MeshFunction("size_t", mesh, 1)
facetboundaries = MeshFunction("size_t",mesh, 2)


VQuadelem = VectorElement("DG", mesh.ufl_cell(), degree=1, quad_scheme="default")
VQuadelem._quad_scheme = 'default'
fiberFS = FunctionSpace(mesh, VQuadelem)

# fiber, sheet and sheet normal directions
ef = Function(fiberFS)
es = Function(fiberFS)
en = Function(fiberFS)

# circumferential, longitudinal and radial directions
ec = Function(fiberFS)
el = Function(fiberFS)
er = Function(fiberFS)

e1 = Constant((1.0, 0.0, 0.0))
e2 = Constant((0.0, 1.0, 0.0))
e3 = Constant((0.0, 0.0, 1.0))

f.read(facetboundaries, meshname+"/"+"facetboundaries")
f.read(edgeboundaries, meshname+"/"+"edgeboundaries")
f.read(ef, meshname+"/"+"eF")
f.read(es, meshname+"/"+"eS")
f.read(en, meshname+"/"+"eN")
f.read(ec, meshname+"/"+"eC")
f.read(el, meshname+"/"+"eL")
f.read(er, meshname+"/"+"eR")
f.close()

ds = dolfin.ds(domain = mesh, subdomain_data = facetboundaries)
dx = dolfin.dx(domain = mesh)
##############################################################################


comm = mesh.mpi_comm()
comm = comm.tompi4py()

topid = 4
endoid = 2
epiid = 1
N = FacetNormal (mesh)
X = SpatialCoordinate(mesh)
Kspring = Constant(0.0)
Press = Expression(("P"), P=0.0,degree=2)
V = VectorFunctionSpace(mesh, 'CG', 2)
TF = TensorFunctionSpace(mesh, 'DG', 1)

Telem2 = TensorElement("Quadrature",mesh.ufl_cell(),degree=deg,shape=2*(3,),quad_scheme="default")
Telem2._quad_scheme = 'default'
for e in Telem2.sub_elements():
	e._quad_scheme = 'default'

Telem4 = TensorElement("Quadrature",mesh.ufl_cell(),degree=deg,shape=4*(3,),quad_scheme="default")
Telem4._quad_scheme = 'default'
for e in Telem4.sub_elements():
	e._quad_scheme = 'default'

Q = FunctionSpace(mesh,'CG',1)


Velem = VectorElement("CG", mesh.ufl_cell(), 2,quad_scheme="default")
Velem._quad_scheme = 'default'
Qelem = FiniteElement("CG",mesh.ufl_cell(),1,quad_scheme="default")
Qelem._quad_scheme = 'default'

W = FunctionSpace(mesh,MixedElement([Velem,Qelem]))



endoring = pick_endoring_bc()(edgeboundaries, 2) 

# fix endoring in x, y, z directions
bcedge = DirichletBC(W.sub(0), Expression(("0.0", "0.0", "0.0"), degree = 2), endoring, method="pointwise")

# fix top in z direction
bctop2 = DirichletBC(W.sub(0).sub(2), Expression(("0.0"), degree = 1), facetboundaries, topid)


bcs = [bctop2,bcedge]


du, dp = TrialFunctions(W)
dw = TrialFunction(W)
w = Function(W)
u, pmatrix = split(w)
v, m = TestFunctions(W)

wtest = TestFunction(W)
J = det(Fmat(u))

n = J*inv(Fmat(u).T)*N


Ea = Emat(u)

Eff = inner(ef, Ea*ef)/inner(ef,ef)
Enn = inner(en, Ea*en)/inner(en,en)
Ess = inner(es, Ea*es)/inner(es,es)
Efs = inner(ef, Ea*es)/sqrt(inner(ef,ef))/sqrt(inner(es,es))
Efn = inner(ef, Ea*en)/sqrt(inner(ef,ef))/sqrt(inner(en,en))
Esn = inner(es, Ea*en)/sqrt(inner(es,es))/sqrt(inner(en,en))
Elocal = as_tensor([[Eff, Efs, Efn], [Efs, Ess, Esn], [Efn, Esn, Enn]])

Etrans = project(Elocal,TF)


I1 = tr(Fmat(u).T*Fmat(u))

# Neo-Hookean material for ground matrix
Wneo = 0.274*0.63*0.5*(I1-3)*dx - pmatrix*(J-1)*dx



I4 = 2*Elocal[0,0]+1
alpha = sqrt(I4)    
# muscle fiber strain energy function
Wmus = 0.7*0.38673*(exp(9.146683*(alpha-1.0)**2)-1)*dx


# read tangent C++ code for muscle fibers
header_file1 = open("Muscle_CPP/stress_simple1.cpp","r")
stresscode_mus = header_file1.read()
header_file2 = open("Muscle_CPP/tangent_simple1.cpp","r")
tangentcode_mus = header_file2.read()

# read tangent C++ code for collagen fibers
header_file3 = open("Collagen_CPP/stress_simple1.cpp","r")
stresscode_col = header_file3.read()
header_file4 = open("Collagen_CPP/tangent_simple1.cpp","r")
tangentcode_col = header_file4.read()

# C++ code for calculate the tortuosity
header_file5 = open("Tortuosity/tortuosity1.cpp","r")
tortuosity_code = header_file5.read()

Tortuosity = dolfin.Expression(tortuosity_code,element=Telem2)
Tortuosity.U = Etrans

TransMatrix = as_tensor(ef[i]*e1[j], (i,j))/sqrt(inner(ef,ef))/sqrt(inner(e1,e1)) + as_tensor(es[i]*e2[j], (i,j))/sqrt(inner(es,es))/sqrt(inner(e2,e2)) + as_tensor(en[i]*e3[j], (i,j))/sqrt(inner(en,en))/sqrt(inner(e3,e3))



StressTensor_Mus = dolfin.Expression(stresscode_mus, element=Telem2)
StressTensor_Mus.U = Etrans

StressTensor_Col = dolfin.Expression(stresscode_col, element=Telem2)
StressTensor_Col.U = Etrans


StressGlobal_Mus = as_tensor(TransMatrix[i,k]*TransMatrix[j,l]*StressTensor_Mus[k,l],(i,j))
StressGlobal_Col = as_tensor(TransMatrix[i,k]*TransMatrix[j,l]*StressTensor_Col[k,l],(i,j))

TangentTensor_Mus = dolfin.Expression(tangentcode_mus,  element=Telem4)
TangentTensor_Mus.U = Etrans

TangentTensor_Col = dolfin.Expression(tangentcode_col,  element=Telem4)
TangentTensor_Col.U = Etrans

p, q, r, s = indices(4)
TangentGlobal_Mus = as_tensor(TransMatrix[p,i]*TransMatrix[q,j]*TransMatrix[r,k]*TransMatrix[s,l]*TangentTensor_Mus[i,j,k,l], (p,q,r,s))
TangentGlobal_Col = as_tensor(TransMatrix[p,i]*TransMatrix[q,j]*TransMatrix[r,k]*TransMatrix[s,l]*TangentTensor_Col[i,j,k,l], (p,q,r,s))



############# Weak form #############################################
Fneo = derivative(Wneo,w,wtest)
Jac_neo = derivative(Fneo,w,dw)


F_mus = derivative(Wmus,w,wtest)
Jac_mus = derivative(F_mus,w,dw)

F2 = Press*inner(n, v)*ds(endoid) 
#F_mus = inner(Fmat(u)*StressGlobal_Mus, grad(v))*dx
F_col = inner(Fmat(u)*StressGlobal_Col, grad(v))*dx


Ftotal = F2 + Fneo + F_mus + F_col 
matrix = 0.5*(grad(du).T*Fmat(u) + Fmat(u).T*grad(du))
result_mus = as_tensor(TangentGlobal_Mus[i,j,k,l]*matrix[k,l],(i,j))
result_col = as_tensor(TangentGlobal_Col[i,j,k,l]*matrix[k,l],(i,j))

#Jac_mus = inner(grad(du)*StressGlobal_Mus, grad(v))*dx + grad(v)[i,j]*Fmat(u)[i,k]*result_mus[k,j]*dx
Jac_col = inner(grad(du)*StressGlobal_Col, grad(v))*dx + grad(v)[i,j]*Fmat(u)[i,k]*result_col[k,j]*dx



Jac2 = derivative(F2, w, dw) 
Jac = Jac2 + Jac_neo + Jac_mus + Jac_col 

# calculate the LV cavity volume 
vol_form = -Constant(1.0/3.0) * inner(det(Fmat(u))*dot(inv(Fmat(u)).T, N), X + u)*ds(endoid)

####### Solve Passive Inflation problem ###################################################################

abs_tol = 1.0E-9
rel_tol = 1.0E-10
maxiter = 25
omega = 1.0
sigma_array = []
lmbda_array = []
sigma_real = []
lmbda = 1.0

PVfile = open("PV_Mean.csv", "w")

print "Number of Cells:", mesh.num_cells()
print >> PVfile, "Pressure(mmHg)", "Volume(ml)"
print >>PVfile, Press.P, assemble(vol_form, form_compiler_parameters={"representation":"uflacs"})
solution = HDF5File(mesh.mpi_comm(), "solution.hdf5", 'w')


for lmbda_value in range(0,330):

        it = 0
        print "lmbda value = ", lmbda_value

        Press.P = Press.P+ 0.01

        if(lmbda_value == 0):

                A, b = assemble_system(Jac, -Ftotal, bcs, form_compiler_parameters={"representation":"uflacs"})
                resid0 = b.norm("l2")
                rel_res = b.norm("l2")/resid0
                res = resid0
                print "Residual: %.3e, Relative residual: %.3e" %(res, rel_res)
                solve(A, w.vector(), b)
        Ea = Emat(w.sub(0))
    	Eff = inner(ef, Ea*ef)/inner(ef,ef)
    	Enn = inner(en, Ea*en)/inner(en,en)
    	Ess = inner(es, Ea*es)/inner(es,es)
   	Efs = inner(ef, Ea*es)/sqrt(inner(ef,ef))/sqrt(inner(es,es))
    	Efn = inner(ef, Ea*en)/sqrt(inner(ef,ef))/sqrt(inner(en,en))
    	Esn = inner(es, Ea*en)/sqrt(inner(es,es))/sqrt(inner(en,en))
	Elocal = as_tensor([[Eff, Efs, Efn], [Efs, Ess, Esn], [Efn, Esn, Enn]])


        Etrans = project(Elocal, TF)
        StressTensor_Mus.U = Etrans
        TangentTensor_Mus.U = Etrans
        StressTensor_Col.U = Etrans
        TangentTensor_Col.U = Etrans
        
        B = assemble(Ftotal, form_compiler_parameters={"representation":"uflacs"})
        for bc in bcs:
            bc.apply(B)

        rel_res = 1.0
        res = B.norm("l2")
        resid0 = res
        print "Residual: %.3e, Relative residual: %.3e" %(res, rel_res)

        dww = Function(W)
        dww.vector()[:] = 0.0

        while (rel_res > rel_tol and res > abs_tol) and it < maxiter:
                it += 1

                A, b = assemble_system(Jac, -Ftotal, bcs, form_compiler_parameters={"representation":"uflacs"})
                solve(A, dww.vector(), b)
                w.vector().axpy(1.0, dww.vector())
                Ea = Emat(w.sub(0))
    		Eff = inner(ef, Ea*ef)/inner(ef,ef)
    		Enn = inner(en, Ea*en)/inner(en,en)
    		Ess = inner(es, Ea*es)/inner(es,es)
    		Efs = inner(ef, Ea*es)/sqrt(inner(ef,ef))/sqrt(inner(es,es))
    		Efn = inner(ef, Ea*en)/sqrt(inner(ef,ef))/sqrt(inner(en,en))
    		Esn = inner(es, Ea*en)/sqrt(inner(es,es))/sqrt(inner(en,en))
		Elocal = as_tensor([[Eff, Efs, Efn], [Efs, Ess, Esn], [Efn, Esn, Enn]])


                Etrans = project(Elocal, TF)
        	StressTensor_Mus.U = Etrans
        	TangentTensor_Mus.U = Etrans
        	StressTensor_Col.U = Etrans
        	TangentTensor_Col.U = Etrans


                B = assemble(Ftotal, form_compiler_parameters={"representation":"uflacs"})
                for bc in bcs:
                    bc.apply(B)

                rel_res = B.norm("l2")/resid0
                res = B.norm("l2")
 
                print "2Residual: %.3e, Relative residual: %.3e" %(res, rel_res)

        print "volume = ", assemble(vol_form, form_compiler_parameters={"representation":"uflacs"}), "pressure = ", Press.P*7.5

        w.sub(0).rename("u_disp","u_disp")
        w.sub(1).rename("p","p")
        
        solution.write(w.sub(0), "u_disp/u_{}".format(lmbda_value) )
        solution.write(w.sub(1), "p/p_{}".format(lmbda_value))
 
       
        print >> PVfile, Press.P, assemble(vol_form, form_compiler_parameters={"representation":"uflacs"})

solution.close()
print datetime.now()-startTime
