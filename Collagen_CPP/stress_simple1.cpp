#include<functional>


namespace dolfin
{
  std::vector<std::size_t> shape = {3,3};
class MyFunc : public Expression
{

  mutable Array<double> UX;

public:

  std::shared_ptr<const Function> U;

  MyFunc() : Expression(shape), UX(9)
  {
  }
  static inline double sintegral(std::function<double(double)> f,double a, double b, int n);
  static inline double sJacobian(double xbar, double ybar, double zbar);
  static inline double sf2(double ybar,double zbar,double E11,double E22,double E33,double E12,double E21,double E13,double E31,double E23,double E32);
  static inline double sR(double ybar,double zbar);
  void eval(Array<double>& values, const Array<double>& x) const;
 
};


double MyFunc::sintegral(std::function<double(double)>f, double a, double b, int n)
{
   double h = (b-a)/n;
   double k = 0.0; 
   for (int i = 0; i<n; i++)
   {
     k += f(a+(i+0.5)*h)*h;
   }
   return k;
}

double MyFunc::sR(double ybar, double zbar)
{
   const double pi = 3.141592653589793238463;
   double a = 0.0;
   double b = pi*ybar + pi;
   double c = pi/2*zbar + pi/2;
   double sigmaphi = 0.1;
   double mphi = 0.9271;     
//   double phi = arccos(sin(a)*cos(b)*sin(c)+cos(a)*cos(c));
   return 1/(2*pi)*(1/(2*sqrt(2*pi)*sigmaphi)*exp(-pow(mphi-c,2.0)/(2*pow(sigmaphi,2.0))) + 1/(2*sqrt(2*pi)*sigmaphi)*exp(-pow(pi-mphi-c,2.0)/(2*pow(sigmaphi,2.0))));

}

double MyFunc::sJacobian(double xbar, double ybar, double zbar)
{
   const double pi = 3.141592653589793238463;
   
   double a = pi/24*xbar;
   double b = pi*ybar + pi;
   double c = pi/2*zbar + pi/2;
   double temp1 = cos(c)*sin(a) - cos(a)*cos(b)*sin(c);
   
   double d1 = (-((cos(b)*sin(c))/temp1 - (cos(a)*pow(sin(b),2.0)*pow(sin(c),2.0))/pow(temp1,2.0))/((pow(sin(b),2.0)*pow(sin(c),2.0))/pow(temp1,2.0) + 1.0));
   double d2 = (-((cos(c)*sin(b))/temp1 + (sin(b)*sin(c)*(sin(a)*sin(c) + cos(a)*cos(b)*cos(c)))/pow(temp1,2.0))/((pow(sin(b),2.0)*pow(sin(c),2.0))/pow(temp1,2.0) + 1.0));
   double d3 = (sin(a)*sin(b)*sin(c))/sqrt(1.0 - pow(cos(a)*cos(c) + cos(b)*sin(a)*sin(c),2.0));
   double d4 = (cos(a)*sin(c) - cos(b)*cos(c)*sin(a))/sqrt(1.0 - pow(cos(a)*cos(c) + cos(b)*sin(a)*sin(c),2.0));
   double jacobian = d1*d4 - d2*d3;
   
   return jacobian;
}

double MyFunc::sf2(double ybar, double zbar, double E11, double E22, double E33, double E12, double E21, double E13, double E31, double E23,double E32)
{
   const double pi = 3.141592653589793238463;
   const double C2 = 5093;
   const double sigma2 = 0.07703;
   const double m2 = 0.122563;

   double a = 0.0;
   double b = pi*ybar + pi;
   double c = pi/2*zbar + pi/2;

   
   double temp1 = cos(a)*cos(c)-cos(b)*sin(a)*sin(c);
   double temp2 = cos(c)*sin(a)+cos(a)*cos(b)*sin(c);
   double temp3 = sin(b)*sin(c);
   double upper = E11*pow(temp1,2.0) + E22*pow(temp2,2.0) + E33*pow(temp3,2.0) + E12*temp1*temp2 + E21*temp1*temp2
                     + E13*temp1*temp3 + E31*temp1*temp3 + E23*temp2*temp3 + E32*temp2*temp3;

   
   
   std::function<double(double)> invexp = [pi,C2,sigma2,m2,upper] (double x) 
   { 

      return 1.0/sqrt(2.0*pi)/sigma2/(1-0.5*(1+erf(-m2/sigma2/sqrt(2))))*exp(-pow(m2-x,2.0)/(2.0*pow(sigma2,2.0)))*C2*(upper-x)/(1.0+2.0*x);
   };
   
   if (upper < 1e-6) { 
        return 0.0;}
   else {
   
         double result = sintegral(invexp, 0.0, upper, 10);

         return result;}
}




void MyFunc::eval(Array<double>& values, const Array<double>& x) const
{
    //MyFunc A;
    U->eval(UX, x);
    double E11 = UX[0];
    double E12 = UX[1];
    double E13 = UX[2];
    double E21 = UX[3];
    double E22 = UX[4];
    double E23 = UX[5];
    double E31 = UX[6];
    double E32 = UX[7];
    double E33 = UX[8];


    double S11 = 0.0;
    double S22 = 0.0;
    double S33 = 0.0;
    double S12 = 0.0;
    double S13 = 0.0;
    double S23 = 0.0;
    
    const double pi = 3.141592653589793238463;
    double s2 = 0.0263;
//    double weights[10] = {0.2955242247147529,0.2955242247147529,0.2692667193099963,0.2692667193099963,0.2190863625159820,0.2190863625159820,0.1494513491505806,0.1494513491505806,0.0666713443086881,0.0666713443086881};
//    double points[10] = {-0.1488743389816312,0.1488743389816312,-0.4333953941292472,0.4333953941292472,-0.6794095682990244,0.6794095682990244,-0.8650633666889845,0.8650633666889845,-0.9739065285171717,0.9739065285171717};
//    double weights[5] = {0.5688888888888889,0.4786286704993665,0.4786286704993665,0.2369268850561891,0.2369268850561891};
//    double points[5] = {0.0000000000000000,-0.5384693101056831,0.5384693101056831,-0.9061798459386640,0.9061798459386640};

//    double weights[20] = {0.152753387130726,0.152753387130726,0.149172986472604,0.149172986472604,0.142096109318382,0.142096109318382,0.131688638449177,0.131688638449177,0.118194531961518,0.118194531961518,0.101930119817240,0.101930119817240,0.083276741576705,0.083276741576705,0.062672048334109,0.062672048334109,0.040601429800387,0.040601429800387,0.017614007139152,0.017614007139152};
//    double points[20] = {-0.076526521133497,0.076526521133497,-0.227785851141645,0.227785851141645,-0.373706088715419,0.373706088715419,-0.510867001950827,0.510867001950827,-0.636053680726515,0.636053680726515,-0.746331906460151,0.746331906460151,-0.839116971822219,0.839116971822219,-0.912234428251326,0.912234428251326,-0.963971927277914,0.963971927277914,-0.993128599185095,0.993128599185095};
    double weights[30] = {0.1028526528935588,0.1028526528935588,0.1017623897484055,0.1017623897484055,0.0995934205867953,0.0995934205867953,0.0963687371746443,0.0963687371746443,0.0921225222377861,0.0921225222377861,0.0868997872010830,0.0868997872010830,0.0807558952294202,0.0807558952294202,0.0737559747377052,0.0737559747377052,0.0659742298821805,0.0659742298821805,0.0574931562176191,0.0574931562176191,0.0484026728305941,0.0484026728305941,0.0387991925696271,0.0387991925696271,0.0287847078833234,0.0287847078833234,0.0184664683110910,0.0184664683110910,0.0079681924961666,0.0079681924961666};
    double points[30] = {-0.0514718425553177,0.0514718425553177,-0.1538699136085835,0.1538699136085835,-0.2546369261678899,0.2546369261678899,-0.3527047255308781,0.3527047255308781,-0.4470337695380892,0.4470337695380892,-0.5366241481420199,0.5366241481420199,-0.6205261829892429,0.6205261829892429,-0.6978504947933158,0.6978504947933158,-0.7677774321048262,0.7677774321048262,-0.8295657623827684,0.8295657623827684,-0.8825605357920527,0.8825605357920527,-0.9262000474292743,0.9262000474292743,-0.9600218649683075,0.9600218649683075,-0.9836681232797472,0.9836681232797472,-0.9968934840746495,0.9968934840746495};
    int i,j;
    for (i = 0; i < 30; i++)
        { 
          for (j = 0; j < 30; j++)
              {
             
                       double temp0 = pi*pi/2*weights[i]*weights[j];
                       double a = 0.0;
                       double b = pi*points[i] + pi;
                       double c = pi/2*points[j] + pi/2;
                       double temp11 = cos(a)*cos(c)-cos(b)*sin(a)*sin(c);
                       double temp22 = cos(c)*sin(a)+cos(a)*cos(b)*sin(c);
                       double temp33 = sin(b)*sin(c);
                       double temp12 = (cos(a)*cos(c)-cos(b)*sin(a)*sin(c))*(cos(c)*sin(a)+cos(a)*cos(b)*sin(c));
                       double temp13 = sin(b)*sin(c)*(cos(a)*cos(c)-cos(b)*sin(a)*sin(c));
                       double temp23 = sin(b)*sin(c)*(cos(c)*sin(a)+cos(a)*cos(b)*sin(c));
 

                       double fiber_angle = sR(points[i],points[j]);
                       double waviness = sf2(points[i], points[j],E11,E22,E33,E12,E21,E13,E31,E23,E32);
                       S11 += temp0*s2*pow(temp11,2.0)*fiber_angle*waviness; 
                       S22 += temp0*s2*pow(temp22,2.0)*fiber_angle*waviness;  
                       S33 += temp0*s2*pow(temp33,2.0)*fiber_angle*waviness; 
                       S12 += temp0*s2*temp12*fiber_angle*waviness; 
                       S13 += temp0*s2*temp13*fiber_angle*waviness; 
                       S23 += temp0*s2*temp23*fiber_angle*waviness; 
             
              }
        }
  


/*    cout << "S11 = " << S11 << endl;
    cout << "S12 = " << S12 << endl;
    cout << "S13 = " << S13 << endl;
    cout << "S23 = " << S23 << endl;
    cout << "S22 = " << S22 << endl;
    cout << "S33 = " << S33 << endl;*/
//    cout << "S11 = " << S11 << endl;                      
    values[0] = S11;
    values[1] = S12;
    values[2] = S13;

    values[3] = S12;
    values[4] = S22;
    values[5] = S23;

    values[6] = S13;
    values[7] = S23;
    values[8] = S33;

}





};
